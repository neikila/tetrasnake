#include "algo.h"
#include "tinyxml.h"
#include "tinystr.h"
#include <stdlib.h>

size_t *Figure::NumberOfBox;
size_t *Figure::NumberOfForms;
size_t Figure::SizeOf_NumberOfForms;
Point**** Figure::mas;		

void Swap(int* a, int* b)
{
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}

size_t CountSym(const char *string, char Sym)
{
	size_t count = 0;
	while (*string)
		if (*string++ == Sym)
			++count;
	return count;
}

/* 	Field		*/
Field::Field(int xx = 0, int yy = 0) : 
SizeX(xx), SizeY(yy) 
{
};

int Field::GetX()
{
	return SizeX;
}

int Field::GetY()
{
	return SizeY;
}

/*	Point 		*/
Point::Point(int xx = 0, int yy = 0) : 
X(xx), Y(yy) 
{
};

int Point::GetX()
{
	return X;
}

int Point::GetY()
{
	return Y;
}

Point Point::operator + (Point A)
{
	return Point(X + A.X, Y + A.Y);
}

Point Point::operator - (Point A)
{
	return Point(X - A.X, Y - A.Y);
}

void Point::Swap(Point& A)
{
	::Swap(&X, &(A.X) );
	::Swap(&Y, &(A.Y) ); 
}

/*	Snake		*/
Snake::Snake(size_t SSize = 2, size_t MMaxSize = 7) :
Direction(2), Size(SSize), MaxSize(MMaxSize), Head(0)
{
	if (Size > 4)
		Size = 4;
	Mas = new Point[MMaxSize];
	for (int i = 0; i < Size; ++i)
	{
		Mas[i] = Point(5 + i,5);
	}
}

int Snake::GetDirection()
{
	return Direction;
}

size_t Snake::GetSize()
{
	return Size;
}

Point Snake::operator[] (int Index)
{
	return Mas[ (Index + Head) % Size ];
}

void Snake::Move()
{
	size_t DeltaX;
	size_t DeltaY;
	switch (Direction)
	{
		case 0: DeltaX = 1; DeltaY = 0; break;
		case 1: DeltaX = 0; DeltaY = 1; break;
		case 2: DeltaX = -1; DeltaY = 0; break;
		case 3: DeltaX = 0; DeltaY = -1; break;
	}
	size_t Second = Head;
	Head = (Head == 0? Size: Head) - 1;
	Mas[Head] = Mas[Second] + Point(DeltaX, DeltaY);
}

void Snake::Turn(char c)
{
	switch (c)
	{
		case 'r': Direction = 0; break;
		case 'u': Direction = 1; break;
		case 'l': Direction = 2; break;
		case 'd': Direction = 3; break;
	}
}

/*	Figure 		*/
Point Figure::operator[] (int Index)
{
	return *(Mas[Index]);
}

bool Figure::Fullmas(const char* Filename)
{
	TiXmlDocument *xml_file = new TiXmlDocument(Filename);
	if(!xml_file->LoadFile())
		return false;

	char string[5];
	string[4] = '\0';
	TiXmlElement *xml_level = 0;
	TiXmlElement *xml_body = 0, *xml_body_temp = 0;
	TiXmlElement *xml_entity = 0;

	xml_level = xml_file->FirstChildElement("level");
	xml_entity = xml_level->FirstChildElement("entity");

	size_t NumberOfRow = 0;
	SizeOf_NumberOfForms = 0;
	while( xml_entity != NULL )
	{
		++SizeOf_NumberOfForms;
		xml_entity = xml_entity->NextSiblingElement("entity");
	}

	NumberOfForms = new size_t [SizeOf_NumberOfForms];
	NumberOfBox = new size_t [SizeOf_NumberOfForms];
	mas = new Point*** [SizeOf_NumberOfForms];

	xml_entity = xml_level->FirstChildElement("entity");
	size_t IndexOfFigure = 0; 	//Индекс сущности
	while( xml_entity != NULL )
	{
		NumberOfBox[IndexOfFigure] = 0;	//Количество квадратов в фигуре
		xml_body = xml_entity->FirstChildElement("body");
		NumberOfForms[IndexOfFigure] = atoi(xml_body->Attribute("NumberOfRotations"));
		xml_body = xml_body_temp = xml_body->NextSiblingElement("body");
		while ( xml_body != NULL )	//Расчет общего количества точек в форме
		{
			sprintf(string,"%s",xml_body->Attribute("line"));
			string[4] = '\0';
			NumberOfBox[IndexOfFigure] += CountSym(string, '#');
			xml_body = xml_body->NextSiblingElement("body");
		}

		mas[IndexOfFigure] = new Point**[NumberOfForms[IndexOfFigure]];

		for (size_t k = 0; k < NumberOfForms[IndexOfFigure]; k++) //Выделение памяти под каждую точку для каждой формы конкретной фигуры
		{
			mas[IndexOfFigure][k]= new Point* [NumberOfBox[IndexOfFigure]];
			for (size_t j = 0; j < NumberOfBox[IndexOfFigure]; j++)
			{
				mas[IndexOfFigure][k][j] = new Point;
			}
		}
		xml_body = xml_body_temp;
		NumberOfRow = 0;
		size_t IndexOfBox = 0;
		while ( xml_body != NULL )			//Задание расположения элементов фигуры для начального положения
		{
			++NumberOfRow;
			sprintf(string,"%s",xml_body->Attribute("line"));
			string[4] = '\0';
			for (size_t i = 0; i < 4; ++i)
				if (string[i] == '#')
				{
					*mas[IndexOfFigure][0][IndexOfBox] = Point((int)(i - 1), (int)(4 - NumberOfRow - 1));
				}
			xml_body = xml_body->NextSiblingElement("body");
		}
		xml_entity = xml_entity->NextSiblingElement("entity");
		++IndexOfFigure;
	}
	/*for (int i = 0; i < 7; ++i)
		for (int k=1; k<NumberOfForms[i]; k++)
			for (int j=0;j<4;j++)
				(*mas[i][k][j])=mas[i][k-1][j]->rotate();*/
}
		

/*	Heap		*/
/*	Player		*/
