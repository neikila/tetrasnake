#include "tinyxml.h"
#include "tinystr.h"
#include <stdlib.h>

bool GetSettings(int *step, int *n_x, int *n_y, int *sphere_num, int *speed, int* radius)
{
	TiXmlDocument *xml_file = new TiXmlDocument("settings.xml");
	if(!xml_file->LoadFile())
		return false; 

	TiXmlElement *xml_level = 0;
	TiXmlElement *xml_body = 0;
	TiXmlElement *xml_entity = 0;

	xml_level = xml_file->FirstChildElement("level");

	xml_entity = xml_level->FirstChildElement("entity");
	xml_body = xml_entity->FirstChildElement("body");

	printf("1) %s\n", xml_entity->Attribute("class"));
	*step = atoi(xml_body->Attribute("step"));
	printf("Step = %d\n", *step);
	*n_x = atoi(xml_body->Attribute("N_X"));
	printf("Size_X = %d\n", *n_x);
	*n_y = atoi(xml_body->Attribute("N_Y"));
	printf("Size_Y = %d\n", *n_y);

	xml_entity = xml_entity->NextSiblingElement("entity");
	xml_body = xml_entity->FirstChildElement("body");

	printf("2) %s\n", xml_entity->Attribute("class"));
	*sphere_num = atoi(xml_body->Attribute("sphereamount"));
	printf("Sphereamount = %d\n", *sphere_num);
	*speed = atoi(xml_body->Attribute("speed"));
	printf("Speed = %d\n", *speed);
	*radius = atoi(xml_body->Attribute("radius"));
	printf("Radius = %d\n", *radius);
	return true;
}
