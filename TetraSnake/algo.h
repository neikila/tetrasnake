#include <stdlib.h>

//Паттерн изменение уровня сложности
//Изменение заключается в ликидации возможности поворота некоторой фигуры в одно из положений: Так если у зикзага два положения изначально - останется одно

class Field
{
private:
	Field(int, int);
	int GetX();
	int GetY();
public:
	int SizeX;		//0,0 - координаты левого нижнего угла
	int SizeY;
};

class Point
{
public:
	Point(int, int);
	int GetX();
	int GetY();
	Point operator + (Point);
	Point operator - (Point);
	void Swap(Point&);
private:
	int X;			//Координаты задаются относительно поля
	int Y;
};

class Snake
{
private:
	Snake(size_t, size_t);
	int GetDirection();	//Получить направление
	size_t GetSize();
	Point operator[] (int);	//Получение точку змейки
	void Move();		//Переместить змейку на одну клеточку в соответсвии с заданым направлением
	void Turn(char);	//Повернуть змейку. Передается сигнал.
public:
	int Direction;		//Направление движение змейки: 0 - право, 1 - вверх, 2 - влево, 3 - вниз. Направление задается относительно взгляда человека на экран.
	size_t Size; 		//Длина змейки
	size_t MaxSize; 	//Максимальная длинна змейк (ограничение)
	Point* Mas; 		//Массив точек, характеризующих положение змейки (каждой её части) в поле
	size_t Head;		//Указатель на голову змейки
};

class Figure
{
public:
	static bool Fullmas(const char*);
	size_t GetSize();
	Point operator[] (int);	//Получение точку фигуры
	void Move();		//Переместить змейку на одну клеточку в соответсвии с заданым направлением
	void Turn(char);	//Повернуть змейку. Передается сигнал.	
private:
	size_t Size;		//Количество блоков, из которых состоит элемент
	static size_t *NumberOfForms;	//Количество вариантов поворота у фигуры
	static size_t *NumberOfBox;	//Количество квадратов в фигуре
	static size_t SizeOf_NumberOfForms;
	static Point ****mas;	//Массив точек, характеризующих положение фигуры (каждой её части) в поле
	size_t Position;
	size_t Type;
};

class Heap
{
public:
	size_t GetSize();
	Point operator[] (int);	//Получение точку кучи
	int Check();
private:
	size_t Size;		//Количество блоков, из которых состоит куча
	Point** Mas;		//Массив точек, характеризующих положение кучи (каждой её части) в поле
};

class Player
{
public:
	Heap* GetHeap();
	Snake* GetSnake();
	Figure* GetFigure();
private:
	Figure* Fig;
	Snake* Sn;
	Heap* Hp;
	int Score;		//Счет игрока
};
